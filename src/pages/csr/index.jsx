import Layout from "@/components/Layout";
import {
    QueryClient,
    QueryClientProvider,
    useQuery
  } from "@tanstack/react-query";

const queryClient = new QueryClient()

export default function HomeCsrIndex() {
    return (
        <QueryClientProvider client={queryClient}>
            <HomeCsr />
        </QueryClientProvider>
    );
}
function HomeCsr() {

    const fetchData = async () => {
        const data = await fetch("https://api.alquran.cloud/v1/surah")
        .then((response) => {
            return response.json();
        })
        .catch((err) => {
            alert(err);
        });

        return data
    }

    const { isPending, error, data } = useQuery({
        queryKey: ["surahData"],
        queryFn: fetchData,
    });

    if (error){
        return alert(error.message);
    }

    return (
        <Layout>
            <section id="features" className="features">
                <div className="container">
                    <div className="section-title">
                        <h2>App Features</h2>
                        <p>
                            Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                            ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                            quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                            Quia fugiat sit in iste officiis comdi quidem hic quas.
                        </p>
                    </div>
                    <div className="row no-gutters">
                       {isPending ? (
                        <>
                            <h6>Loading</h6>
                        </>
                       ) : (
                        <>
                            {data?.data?.map((value) => (
                                <>
                                    <h6>{value.englishName}</h6>
                                </>
                            ))}
                        </>
                       )}
                    </div>
                </div>
            </section>
        </Layout>
    )
}