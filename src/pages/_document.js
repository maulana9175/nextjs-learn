import { Html, Head, Main, NextScript } from "next/document";

export default function Document() {
  return (
    <Html lang="en">
      <Head>
        <meta charSet="utf-8" />
        <meta content="width=device-width, initial-scale=1.0" name="viewport" />
        <title>Appland Bootstrap Template - Index</title>
        <meta content name="description" />
        <meta content name="keywords" />
        <link href="/template/assets/img/favicon.png" rel="icon" />
        <link
          href="/template/assets/img/apple-touch-icon.png"
          rel="apple-touch-icon"
        />
        <link
          href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i"
          rel="stylesheet"
        />
        <link href="/template/assets/vendor/aos/aos.css" rel="stylesheet" />
        <link
          href="/template/assets/vendor/bootstrap/css/bootstrap.min.css"
          rel="stylesheet"
        />
        <link
          href="/template/assets/vendor/bootstrap-icons/bootstrap-icons.css"
          rel="stylesheet"
        />
        <link
          href="/template/assets/vendor/boxicons/css/boxicons.min.css"
          rel="stylesheet"
        />
        <link
          href="/template/assets/vendor/glightbox/css/glightbox.min.css"
          rel="stylesheet"
        />
        <link
          href="/template/assets/vendor/swiper/swiper-bundle.min.css"
          rel="stylesheet"
        />
        <link href="/template/assets/css/style.css" rel="stylesheet" />
      </Head>
      <body className="sidebar-mini">
        <Main />
        <NextScript />
        <script src="/template/assets/vendor/aos/aos.js"></script>
        <script src="/template/assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="/template/assets/vendor/glightbox/js/glightbox.min.js"></script>
        <script src="/template/assets/vendor/swiper/swiper-bundle.min.js"></script>
        <script src="/template/assets/vendor/php-email-form/validate.js"></script>
        <script src="/template/assets/js/main.js"></script>
      </body>
    </Html>
  );
}
