import Layout from "@/components/Layout";

export async function getServerSideProps() {
    const data = await fetch("https://api.alquran.cloud/v1/surah")
        .then((response) => {
            return response.json();
        })
        .catch((err) => {
            alert(err);
        });

    console.log(data)

    return { props: { data } };
}
export default function HomeSsr({ data }) {
    console.log(data, "INI DTAA")
    return (
        <Layout>
            <section id="features" className="features">
                <div className="container">
                    <div className="section-title">
                        <h2>App Features</h2>
                        <p>
                            Magnam dolores commodi suscipit. Necessitatibus eius consequatur
                            ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
                            quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
                            Quia fugiat sit in iste officiis comdi quidem hic quas.
                        </p>
                    </div>
                    <div className="row no-gutters">
                        {data.data.map((value) => (
                            <>
                                <h6>{value.englishName}</h6>
                            </>
                        ))}
                    </div>
                </div>
            </section>
        </Layout>
    )
}