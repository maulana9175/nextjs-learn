import Layout from "@/components/Layout";
import {
  QueryClient,
  QueryClientProvider,
  useQuery,
} from "@tanstack/react-query";

const client = new QueryClient();
export default function Home() {
  return (
    <QueryClientProvider client={client}>
      <CardComponent />
    </QueryClientProvider>
  );
}

function CardComponent() {
  const fetchData = () => {
    const response = fetch("https://api.alquran.cloud/v1/surah")
      .then((response) => {
        return response.json();
      })
      .catch((err) => {
        alert(err);
      });
    return response;
  };

  const { isPending, error, data } = useQuery({
    queryKey: ["fetchSurah"],
    queryFn: fetchData,
  });

  if (isPending) return "Loading...";

  if (error) return "An error has occurred: " + error.message;

  
  return (
    <Layout>
      <section id="features" className="features">
        <div className="container">
          <div className="section-title">
            <h2>App Features</h2>
            <p>
              Magnam dolores commodi suscipit. Necessitatibus eius consequatur
              ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam
              quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea.
              Quia fugiat sit in iste officiis comdi quidem hic quas.
            </p>
          </div>
          <div className="row no-gutters">
            {isPending ? (
              "Loading..."
            ) : (
              <div className="col-xl-12 d-flex align-items-stretch order-2 order-lg-1">
                <div className="content d-flex flex-column justify-content-center">
                  <div className="row">
                    {data.data.map((value) => (
                      <SuratComponent data={value}/>
                    ))}
                  </div>
                </div>
              </div>
            )}
          </div>
        </div>
      </section>
    </Layout>
  );
}


function SuratComponent({ data }) {
  return (
    <div className="col-md-6 icon-box" data-aos="fade-up" data-aos-delay={100}>
      <i className="bx bx-cube-alt" />
      <h4>({data?.number}) {data?.englishName} - {data?.name}</h4>
      <p>
        Diturunkan di kota : {data?.revelationType} | Jumlah Ayat: {data?.numberOfAyahs}
      </p>
    </div>
  );
}
