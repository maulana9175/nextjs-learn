import {
  QueryClient,
  QueryClientProvider,
  useQuery,
} from "@tanstack/react-query";
import ComponentChild from "./child/home";
const queryClient = new QueryClient();

export default function Dashboard() {
  return (
    <QueryClientProvider client={queryClient}>
      <Example />
    </QueryClientProvider>
  );
}

function Example() {
  const fetchData = () => {
    const data = fetch("https://api.alquran.cloud/v1/edition")
      .then((response) => {
        return response.json();
      })
      .catch((err) => {
        alert(err);
      });

    return data;
  };

  const { isPending, error, data } = useQuery({
    queryKey: ["newProjects"],
    queryFn: fetchData,
  });

  console.log(data);
  if (isPending) return "Loading...";

  if (error) return "An error has occurred: " + error.message;

  return (
    <>
      {data?.data?.map((value) => (
        <ComponentChild title={value.englishName} />
      ))}
    </>
  );
}
