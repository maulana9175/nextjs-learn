const ComponentChild = ({title}) => {
    return (
        <>
            <h6>{title}</h6>
        </>
    )
}

export default ComponentChild;