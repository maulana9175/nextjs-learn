import Header from "./Header";

export default function Layout({ children }) {
  return (
    <>
      <Header />
      <main id="main">{children}</main>
    </>
  );
}
